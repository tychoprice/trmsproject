import { Component, OnInit } from '@angular/core'
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {CookieService} from "ng2-cookies";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  un: string;
  pw: string;

  onSubmit() {
    let reqParams = {
      username: this.un,
      password: this.pw
    }
    return this.http.post<any>('http://localhost:8081/api/login', JSON.stringify(reqParams)).subscribe(e => {
      this.cookie.set('username', e.username);
      console.log(e);
    });
  }

  constructor(

    private http: HttpClient,
    private router: Router,
    private cookie: CookieService
  ) { }

  ngOnInit() {
  }

}
