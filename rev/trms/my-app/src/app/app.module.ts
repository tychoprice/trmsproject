import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { ReimbursementsComponent } from './reimbursements/reimbursements.component';
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {CookieService} from "ng2-cookies";



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    ReimbursementsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule
  ],
  providers: [AppRoutingModule, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
