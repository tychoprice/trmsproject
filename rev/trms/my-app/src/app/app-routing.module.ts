import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {ReimbursementsComponent} from "./reimbursements/reimbursements.component";

const routes: Routes = [
  { path: 'reimbursements', component: ReimbursementsComponent },
  { path: '', component: LoginComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
