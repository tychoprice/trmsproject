import { Component, OnInit } from '@angular/core';
import {CookieService} from "ng2-cookies";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  getUsername(): string {
    if (this.cookie.get("username") != null)
      return this.cookie.get("username");
    else return "";
  }

  

  constructor(
    private cookie: CookieService
  ) { }

  ngOnInit() {
  }

}
