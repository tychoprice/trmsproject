//package servlets;
//
//import models.beans.Reimbursement;
//import models.daos.DAOUtility;
//import models.daos.ReimbursementDAO;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.sql.Date;
//import java.sql.SQLIntegrityConstraintViolationException;
//
//@WebServlet("/addReimbursement")
//public class AddReimbursementServlet extends HttpServlet {
//
//    private static final long serialVersionUID = 1L;
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        request.getRequestDispatcher("addReimbursement.jsp").forward(request, response);
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        //Get Parameters
//        //We MUST convert to a Long since parameters are always Strings
//        int reimbursement_id = Integer.parseInt(request.getParameter("reimbursement_id"));
//
//        String course_type = request.getParameter("course_type");
//        String start_date = request.getParameter("start_date");
//        String location = request.getParameter("location");
//        String description = request.getParameter("description");
//        String reason = request.getParameter("reason");
//        String attachments = request.getParameter("attachments");
//        String work_missed = request.getParameter("work_missed");
//
//        java.util.Date date = new java.util.Date(Long.parseLong(dateOfBirth));
//        Date sqldate = new Date(date.getTime());
//
//        //Create an Reimbursement object from the parameters
//        Reimbursement reimbursementToSave = new Reimbursement(
//                reimbursement_id,
//                course_type,
//                start_date,
//                location,
//                description,
//                reason,
//                attachments,
//                work_missed);
//
//        //Call DAO method
//        ReimbursementDAO dao = DAOUtility.getReimbursementDao();
//        try {
//            dao.newReimbursement(reimbursementToSave);
//            request.getSession().setAttribute("message", "Reimbursement successfully created");
//            request.getSession().setAttribute("messageClass", "alert-success");
//            response.sendRedirect("reimbursementHome");
//
//
//        }catch(SQLIntegrityConstraintViolationException e){
//            e.printStackTrace();
//
//            //change the message
//            request.getSession().setAttribute("message", "Id of " + reimbursementToSave.getReimbursementId() + " is already in use");
//            request.getSession().setAttribute("messageClass", "alert-danger");
//
//            request.getRequestDispatcher("addReimbursement.jsp").forward(request, response);
//
//        }catch (Exception e){
//            e.printStackTrace();
//
//            //change the message
//            request.getSession().setAttribute("message", "There was a problem creating the reimbursement at this time");
//            request.getSession().setAttribute("messageClass", "alert-danger");
//
//            request.getRequestDispatcher("addReimbursement.jsp").forward(request, response);
//
//        }
//    }
//
//}
