package com.revature.trms.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.beans.Employee;
import models.daos.EmployeeDAO;
import models.daos.EmployeeDAOImpl;
import models.services.LoginService;
import models.services.TRMSService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/*")
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public HomeServlet() {
        super();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        handleService(req, res, getServletContext());
    }

    protected void handleService(HttpServletRequest req, HttpServletResponse res, ServletContext context) throws IOException, ServletException{
        TRMSService service = getService(req);
        service.init(context, res, req);
        service.run();
    }

    protected TRMSService getService(HttpServletRequest req) {
        String uri = req.getRequestURI();

        return new LoginService();
    }




}