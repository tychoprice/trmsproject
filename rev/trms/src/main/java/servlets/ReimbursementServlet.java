//package servlets;
//
//import models.beans.Reimbursement;
//import models.daos.DAOUtility;
//import models.daos.ReimbursementDAO;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.sql.Date;
//import java.sql.SQLIntegrityConstraintViolationException;
//
//@WebServlet(
//    name = "Reimbursements",
//    urlPatterns = "/api/reimbursements/*"
//)
//public class ReimbursementServlet extends HttpServlet {
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        String[] url = req.getRequestURI().split("/");
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        //Get Parameters
//        //We MUST convert to a Long since parameters are always Strings
//        int reimbursement_id = Integer.parseInt(req.getParameter("reimbursement_id"));
//
//        String course_type = req.getParameter("course_type");
//        Date start_date = req.getParameter("start_date");
//        String location = req.getParameter("location");
//        String description = req.getParameter("description");
//        String reason = req.getParameter("reason");
//        String attachments = req.getParameter("attachments");
//        String work_missed = req.getParameter("work_missed");
//        String cost = req.getParameter("cost");
//
//        Date sqldate = new Date(date.getTime());
//
//        //Create an Reimbursement object from the parameters
//        Reimbursement reimbursementToSave = new Reimbursement(
//                reimbursement_id,
//                course_type,
//                start_date,
//                location,
//                description,
//                reason,
//                attachments,
//                work_missed,
//                cost);
//
//        //Call DAO method
//        ReimbursementDAO dao = DAOUtility.getReimbursementDao();
//        try {
//            dao.newReimbursement(reimbursementToSave);
//            req.getSession().setAttribute("message", "Reimbursement successfully created");
//            req.getSession().setAttribute("messageClass", "alert-success");
//            res.sendRedirect("reimbursementHome");
//
//
//        } catch (SQLIntegrityConstraintViolationException e) {
//            e.printStackTrace();
//
//            //change the message
//            req.getSession().setAttribute("message", "Id of " + reimbursementToSave.getReimbursementId() + " is already in use");
//            req.getSession().setAttribute("messageClass", "alert-danger");
//
//            req.getRequestDispatcher("addReimbursement.jsp").forward(req, res);
//
//        } catch (Exception e){
//            e.printStackTrace();
//
//            //change the message
//            req.getSession().setAttribute("message", "There was a problem creating the reimbursement at this time");
//            req.getSession().setAttribute("messageClass", "alert-danger");
//
//            req.getRequestDispatcher("addReimbursement.jsp").forward(req, res);
//
//        }
//    }
//
//}
