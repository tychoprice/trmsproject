package models.beans;

import java.util.Date;

public class Reimbursement {

    private int reimbursementId;
    private String courseType;
    private java.sql.Date startDate;
    private String location;
    private String description;
    private String gradingFormat;
    private String reason;
    private int cost;
    private byte attachment;
    private int workMissed;

    //Setters
    public void setReimbursementId(int reimbursementId) {
        this.reimbursementId = reimbursementId;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public void setStartDate(java.sql.Date startDate) {
        this.startDate = startDate;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGradingFormat(String gradingFormat) {
        this.gradingFormat = gradingFormat;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setAttachment(byte attachment) {
        this.attachment = attachment;
    }

    public void setWorkMissed(int workMissed) {
        this.workMissed = workMissed;
    }

    //Getters

    public int getReimbursementId() {
        return reimbursementId;
    }

    public String getCourseType() {
        return courseType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public String getGradingFormat() {
        return gradingFormat;
    }

    public String getReason() {
        return reason;
    }

    public int getCost() {
        return cost;
    }

    public byte getAttachment() {
        return attachment;
    }

    public int getWorkMissed() {
        return workMissed;
    }

}
