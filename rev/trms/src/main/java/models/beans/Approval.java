package models.beans;

import models.ApproverAction;

import java.util.Date;

public class Approval {
    private int approvalId;
    private ApproverAction approverAction;
    private String approver_first_name;
    private String approver_last_name;
    private Date approval_date;
    private String approval_reason;

    public Approval(){
        this.approvalId = 0;
        this.approverAction = null;
        this.approver_first_name = null;
        this.approver_last_name = null;
        this.approval_date = null;
        this.approval_reason = null;
    }

    public Approval(int approvalId, ApproverAction approverAction, String approver_first_name, String approver_last_name, Date approval_date, String approval_reason) {
        this.approvalId = approvalId;
        this.approverAction = approverAction;
        this.approver_first_name = approver_first_name;
        this.approver_last_name = approver_last_name;
        this.approval_date = approval_date;
        this.approval_reason = approval_reason;
    }

    //Setters

    public void setApprovalId(int approvalId) {
        this.approvalId = approvalId;
    }

    public void setApproverAction(ApproverAction approverAction) {
        this.approverAction = approverAction;
    }

    public void setApprover_first_name(String approver_first_name) {
        this.approver_first_name = approver_first_name;
    }

    public void setApprover_last_name(String approver_last_name) {
        this.approver_last_name = approver_last_name;
    }

    public void setApproval_date(Date approval_date) {
        this.approval_date = approval_date;
    }

    public void setApproval_reason(String approval_reason) {
        this.approval_reason = approval_reason;
    }

    //Getters

    public int getApprovalId() {
        return approvalId;
    }

    public ApproverAction getApproverAction() {
        return approverAction;
    }

    public String getApprover_first_name() {
        return approver_first_name;
    }

    public String getApprover_last_name() {
        return approver_last_name;
    }

    public Date getApproval_date() {
        return approval_date;
    }

    public String getApproval_reason() {
        return approval_reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Approval approval = (Approval) o;

        if (approvalId != approval.approvalId) return false;
        if (approverAction != approval.approverAction) return false;
        if (approver_first_name != null ? !approver_first_name.equals(approval.approver_first_name) : approval.approver_first_name != null)
            return false;
        if (approver_last_name != null ? !approver_last_name.equals(approval.approver_last_name) : approval.approver_last_name != null)
            return false;
        if (approval_date != null ? !approval_date.equals(approval.approval_date) : approval.approval_date != null)
            return false;
        return approval_reason != null ? approval_reason.equals(approval.approval_reason) : approval.approval_reason == null;
    }

    @Override
    public int hashCode() {
        int result = approvalId;
        result = 31 * result + (approverAction != null ? approverAction.hashCode() : 0);
        result = 31 * result + (approver_first_name != null ? approver_first_name.hashCode() : 0);
        result = 31 * result + (approver_last_name != null ? approver_last_name.hashCode() : 0);
        result = 31 * result + (approval_date != null ? approval_date.hashCode() : 0);
        result = 31 * result + (approval_reason != null ? approval_reason.hashCode() : 0);
        return result;
    }
}
