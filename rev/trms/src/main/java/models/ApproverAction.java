package models;

public enum ApproverAction {

    INFO_REQUESTED("requested"),
    APPROVED("approved"),
    DENIED("denied");

    private final String id;

    ApproverAction(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }

}
