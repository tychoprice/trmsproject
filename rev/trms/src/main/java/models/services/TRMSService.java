package models.services;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class TRMSService {

    protected ServletContext context;
    protected HttpServletResponse res;
    protected HttpServletRequest req;

    public void init(ServletContext context, HttpServletResponse res, HttpServletRequest req) {
        this.context = context;
        this.res = res;
        this.req = req;
    }

    public abstract void run() throws ServletException, IOException;

}
