package models.services;

import models.beans.Employee;
import models.daos.EmployeeDAO;
import models.daos.EmployeeDAOImpl;

import java.sql.SQLException;

public class EmployeeService {

        private static EmployeeDAO employeeDao = new EmployeeDAOImpl();

        public Employee getEmployeeById(String username) throws SQLException{
            return employeeDao.getEmployeeByName(username);
        }

        public Employee saveEmployee(Employee employee) throws SQLException {
            employeeDao.newEmployee(employee);
            return employee;
        }

        public void updateEmployee(Employee employee) throws SQLException {
            employeeDao.updateEmployee(employee);
        }

        public void deleteEmployee(Employee employee) throws SQLException {
            employeeDao.deleteEmployee(employee);
        }
//
// public List<Employee> getAllEmployees() throws SQLException {
//            return employeeDao.getAllEmployees();
//        }

}

