package models.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.beans.Employee;
import models.daos.EmployeeDAOImpl;

import javax.servlet.ServletException;
import java.io.IOException;

//user req=>front controller=>delegate=>service
//        called by the front controller,skipping delegate
//        -control stmt in front controller

public class LoginService extends TRMSService {

    public void run() throws ServletException, IOException{

        Employee e = new ObjectMapper().readValue(req.getInputStream(), Employee.class);
        EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl();
        req.getRequestURI();

        String un = e.getUsername();
        String pw = e.getPassword();

        if (employeeDAO.getEmployeeByName(e.getUsername()).getPassword().equals(pw)) {
            res.setContentType("application/json");

            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(employeeDAO.getEmployeeByName(e.getUsername()));
            res.getWriter().write(jsonInString);
        }
    }

}
