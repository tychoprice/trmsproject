package models.daos;

import models.beans.Employee;

import java.sql.SQLException;

public interface EmployeeDAO {

    boolean doesEmployeeExist(String username) throws SQLException;
    Employee getEmployeeByName(String username) throws SQLException;
    int newEmployee(Employee employee) throws SQLException;
    void updateEmployee(Employee employee) throws SQLException;
    void deleteEmployee(Employee employee) throws SQLException;

}