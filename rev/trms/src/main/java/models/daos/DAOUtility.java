package models.daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOUtility {

    private static final String CONNECTION_USERNAME = "tychoprice";
    private static final String CONNECTION_PASSWORD = "password";
    private static final String URL = "jdbc:postgresql://revprojectdb.cl2fltvqujh7.us-east-1.rds.amazonaws.com:5432/trmsdb";
    private static EmployeeDAO employeeDAOImpl;
    private static Connection connection;

//    private DAOUtility() {}
//
//    public static Connection getConnection() throws SQLException {
//        if (connection == null) {
//            try {
//                Properties prop = new Properties();
//                prop.load(new FileReader("src/main/resources/database.properties"));
//                return DriverManager.getConnection(prop.getProperty("url"),
//                        prop.getProperty("username"), prop.getProperty("password"));
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return connection;
//        }
//    }
//}

    public static synchronized EmployeeDAO getEmployeeDAO() {

        if (employeeDAOImpl == null) {
            employeeDAOImpl = new EmployeeDAOImpl();
        }
        return employeeDAOImpl;
    }

    static synchronized Connection getConnection() throws SQLException {
        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Could not register driver!");
                e.printStackTrace();
            }
            connection = DriverManager.getConnection(URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
        }

        if (connection.isClosed()) {
            System.out.println("Closing connection...");
            connection = DriverManager.getConnection(URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
        }
        return connection;
    }
}
