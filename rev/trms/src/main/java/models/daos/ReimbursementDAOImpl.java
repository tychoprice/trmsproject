package models.daos;

import models.beans.Reimbursement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDAOImpl implements ReimbursementDAO {

    @Override
    public List<Reimbursement> getAllReimbursements() {
        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();
        Connection conn = null;
        Statement stmt = null;

        try {
            conn = DAOUtility.getConnection();
            stmt = conn.createStatement();
            String sql = "SELECT * FROM REIMBURSEMENTS";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reimbursement e = new Reimbursement();

                e.setReimbursementId(rs.getInt("reimbursement_id"));
                e.setCourseType(rs.getString("course_type"));
                e.setStartDate(rs.getDate("start_date"));
                e.setLocation(rs.getString("location"));
                e.setDescription(rs.getString("description"));
                e.setReason(rs.getString("reason"));
                e.setAttachment(rs.getByte("attachments"));
                e.setWorkMissed(rs.getInt("work_missed"));

                reimbursements.add(e);
            }

            System.out.println(reimbursements);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return reimbursements;
    }

    @Override
    public Reimbursement getReimbursementByName(String username) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = DAOUtility.getConnection();
            String query =
                    "SELECT employee_id, reimbursement_id, course_type, start_date, location, description, " +
                            "reason, attachments, work_missed, approval_id, cost " +
                            "FROM REIMBURSEMENTS NATURAL JOIN EMPLOYEES " +
                            "WHERE username = ?";
            ps = conn.prepareStatement(query);

            ps.setString(1, username);
            ps.executeQuery();

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Reimbursement reimbursement = new Reimbursement();

                reimbursement.setReimbursementId(resultSet.getInt(1));
                reimbursement.setCourseType(resultSet.getString(2));
                reimbursement.setStartDate(resultSet.getDate(3));
                reimbursement.setLocation(resultSet.getString(4));
                reimbursement.setDescription(resultSet.getString(5));
                reimbursement.setReason(resultSet.getString(6));
                reimbursement.setAttachment(resultSet.getByte(7));
                reimbursement.setWorkMissed(resultSet.getInt(8));
                reimbursement.setCost(resultSet.getInt(9));

                return reimbursement;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean doesReimbursementExist(String username) {
        Connection connection = null;
        try {
            connection = DAOUtility.getConnection();
            // Prepare Query
            String query = "SELECT EXISTS(SELECT username "+
                    "FROM Reimbursements NATURAL JOIN EMPLOYEES WHERE username=?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);

            // Get result set
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public int newReimbursement(Reimbursement reimbursement) throws SQLException {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DAOUtility.getConnection();
            String sql = "INSERT INTO REIMBURSEMENTS(username, password, first_name, last_name, email) VALUES(?,?,?,?,?) RETURNING reimbursement_id";

            // Setup PreparedStatement
            ps = connection.prepareStatement(sql);

            // Add parameters from reimbursement into PreparedStatement
            ps.setString(1, reimbursement.getCourseType());
            ps.setDate(2, (Date) reimbursement.getStartDate());
            ps.setString(3, reimbursement.getLocation());
            ps.setString(4, reimbursement.getDescription());
            ps.setString(5, reimbursement.getReason());
            ps.setByte(6, reimbursement.getAttachment());
            ps.setInt(7, reimbursement.getWorkMissed());

            ResultSet rs = ps.executeQuery();

            rs.next();
            return rs.getInt(1);

        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return -1;

            }
        }
    }

    @Override
    public void deleteReimbursement(Reimbursement reimbursement) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet results;

        try {
            connection = DAOUtility.getConnection();

            String reimbursementQuery = "DELETE FROM Reimbursements WHERE reimbursement_id=(?) RETURNING reimbursement_id";

            String approvalQuery = "DELETE FROM Approvals WHERE reimbursement_id=?";

            ps = connection.prepareStatement(reimbursementQuery);
            ps.setInt(1, reimbursement.getReimbursementId());
            ps.executeQuery();

            ps = connection.prepareStatement(approvalQuery);
            ps.setInt(1, reimbursement.getReimbursementId());
            ps.executeQuery();

            results = ps.executeQuery();
            results.next();

        } catch (SQLException e) {
            e.printStackTrace();
//            } finally {
//                try {
//                    if (ps != null)
//                        ps.close();
//                    if (connection != null)
//                        connection.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//            }
        }
    }
//            return results;

    @Override
    public void updateReimbursement(Reimbursement reimbursement) throws SQLException {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DAOUtility.getConnection();
            // Prepare Queries
            String sql = "UPDATE Reimbursements " +
                    "SET course_type=?, start_date=?, location=?, description=?, reason=?, attachments=?, work_missed=?" +
                    "WHERE reimbursement_id=?";

            // Prepare reimbursement query
            ps = connection.prepareStatement(sql);
            ps.setString(1, reimbursement.getCourseType());
            ps.setDate(2, (Date) reimbursement.getStartDate());
            ps.setString(3, reimbursement.getLocation());
            ps.setString(4, reimbursement.getDescription());
            ps.setString(5, reimbursement.getReason());
            ps.setByte(6, reimbursement.getAttachment());
            ps.setInt(7, reimbursement.getWorkMissed());

            // Execute reimbursement query
            ps.execute();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
