package models.daos;

import models.beans.Reimbursement;

import java.sql.SQLException;
import java.util.List;

public interface ReimbursementDAO {

    List<Reimbursement> getAllReimbursements();

    boolean doesReimbursementExist(String username) throws SQLException;
    Reimbursement getReimbursementByName(String username) throws SQLException;
    int newReimbursement(Reimbursement reimbursement) throws SQLException;
    void updateReimbursement(Reimbursement reimbursement) throws SQLException;
    void deleteReimbursement(Reimbursement reimbursement) throws SQLException;

//    Reimbursement getAllReimbursementsByUsername(String username) throws SQLException;
//    int getReimbursementCount() throws SQLException;
//    boolean doesReimbursementExist(int reimbursementId) throws SQLException;
//    List<Reimbursement> getAllReimbursementsByEmployeeId(int employeeId) throws SQLException;
//    Reimbursement getReimbursementById(int reimbursementId) throws SQLException;
//    boolean newReimbursement(Reimbursement reimbursement) throws SQLException;
//    void updateReimbursement(Reimbursement reimbursement) throws SQLException;
//    void deleteReimbursement(Reimbursement reimbursement) throws SQLException;

}