//package models.daos;
//
//import models.ApproverAction;
//import models.beans.Approval;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class ApprovalDAOImpl implements ApprovalDAO {
//
//    @Override
//    public boolean doesApprovalExist(int approvalId) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            // Prepare Query
//            String query = "SELECT EXISTS(" + "SELECT approval_id " +
//                    "FROM Approvals " + "WHERE approval_id=?);";
//            PreparedStatement ps = connection.prepareStatement(query);
//            ps.setInt(1, approvalId);
//
//            // Get result set
//            ResultSet resultSet = ps.executeQuery();
//            if (resultSet.next()) {
//                return resultSet.getBoolean(1);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    @Override
//    public int getApprovalCount() {
//        try (Connection connection = DAOUtility.getConnection()) {
//            // Prepare Query
//            String query = "SELECT COUNT(*) FROM Approvals;";
//            PreparedStatement ps = connection.prepareStatement(query);
//
//            // Get result set
//            ResultSet resultSet = ps.executeQuery();
//            if (resultSet.next()) {
//                return resultSet.getInt(1);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }
//
//    @Override
//    public Approval getApprovalById(int approvalId) throws SQLException{
//        try (Connection connection = DAOUtility.getConnection()) {
//            // Prepare Query
//            String query = "SELECT * " +
//                    "FROM Approvals NATURAL FULL OUTER JOIN ApprovalsJunc " +
//                    "WHERE approval_id=?;";
//            PreparedStatement ps = connection.prepareStatement(query);
//            ps.setInt(1, approvalId);
//
//            // Get result set
//            ResultSet resultSet = ps.executeQuery();
//            Approval approval = new Approval();
//            if (resultSet.next()) {
//                approval.setApprovalId(resultSet.getInt("approval_id"));
//                approval.setApproverAction(ApproverAction.valueOf("approver_action"));
//                    resultSet.getString("approver_action").toUpperCase();
//                approval.setApprover_first_name(resultSet.getString("approver_first_name"));
//                approval.setApprover_last_name(resultSet.getString("approver_last_name"));
//                approval.setApproval_date(resultSet.getDate("approval_date"));
//                approval.setApproval_reason(resultSet.getString("approval_reason"));
//            } while (resultSet.next()) {
//                approval.getEmployees().add(resultSet.getString("employee_id"));
//            }
//            return approval;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public List<Approval> getAllPending() {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String query = "SELECT * " +
//                    "FROM (ApprovalsJunc NATURAL JOIN Approvals) NATURAL JOIN BankUser " +
//                    "WHERE approvalstatus='pending';";
//            PreparedStatement ps = connection.prepareStatement(query);
//
//            return loadApprovals(ps.executeQuery());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public List<Approval> getApprovalsByName(String username) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String query = "SELECT * " +
//                    "FROM (ApprovalsJunc NATURAL JOIN Approvals) NATURAL JOIN BankUser " +
//                    "WHERE username=?;";
//            PreparedStatement ps = connection.prepareStatement(query);
//            ps.setString(1, username);
//
//            return loadApprovals(ps.executeQuery());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public List<Approval> getApprovedApprovalsByName(String username) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String query = "SELECT * " +
//                    "FROM (ApprovalsJunc NATURAL JOIN Approvals) NATURAL JOIN BankUser " +
//                    "WHERE username=? AND approvalstatus='approved';";
//            PreparedStatement ps = connection.prepareStatement(query);
//            ps.setString(1, username);
//
//            return loadApprovals(ps.executeQuery());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public List<Approval> getPendingApprovalsByName(String username) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String query = "SELECT * " +
//                    "FROM (ApprovalsJunc NATURAL JOIN Approvals) NATURAL JOIN BankUser " +
//                    "WHERE username=? AND approvalstatus='pending';";
//            PreparedStatement ps = connection.prepareStatement(query);
//            ps.setString(1, username);
//
//            return loadApprovals(ps.executeQuery());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    private List<Approval> loadApprovals(ResultSet resultSet) throws SQLException {
//        List<Approval> approvals = new ArrayList<>();
//        while (resultSet.next()) {
//            Approval approval = new Approval();
//            approval.setApprovalId(resultSet.getInt("approval_id"));
//            approval.setBalance(resultSet.getDouble("balance"));
//            approval.setApprovalType(
//                    ApprovalType.valueOf(resultSet.getString("approvaltype").toUpperCase()));
//            approval.setApprovalStatus(
//                    ApprovalStatus.valueOf(resultSet.getString("approvalstatus").toUpperCase()));
//            approval.getCustomers().add(resultSet.getString("username"));
//            approvals.add(approval);
//        }
//        return approvals;
//    }
//
//    @Override
//    public void newApproval(Approval approval) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String approvalQuery = "INSERT INTO " +
//                    "Approvals(balance, approvaltype, approvalstatus)" +
//                    "VALUES(?, ?::bankapprovaltype, ?::bankapprovalstatus) " +
//                    "RETURNING approval_id;";
//            String juncQuery = "INSERT INTO ApprovalsJunc(approval_id, username)" +
//                    "VALUES(?, ?);";
//
//            PreparedStatement ps = connection.prepareStatement(approvalQuery);
//            ps.setDouble(1, approval.getBalance());
//            ps.setString(2, approval.getApprovalType().toString());
//            ps.setString(3, approval.getApprovalStatus().toString());
//
//            ResultSet resultSet = ps.executeQuery();
//            if (resultSet.next()) {
//                approval.setApprovalId(resultSet.getInt(1));
//            }
//            executeJuncQueries(connection, juncQuery, approval);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void updateApproval(Approval approval) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String approvalQuery = "UPDATE Approvals " +
//                    "SET balance=?, approvaltype=?::bankapprovaltype, approvalstatus=?::bankapprovalstatus " +
//                    "WHERE approval_id=?;";
//            String juncQuery = "INSERT INTO ApprovalsJunc(approval_id, username)" +
//                    "VALUES(?, ?) ON CONFLICT (approval_id, username) DO NOTHING;";
//
//            PreparedStatement ps = connection.prepareStatement(approvalQuery);
//            ps.setDouble(1, approval.getBalance());
//            ps.setString(2, approval.getApprovalType().toString());
//            ps.setString(3, approval.getApprovalStatus().toString());
//            ps.setInt(4, approval.getApprovalId());
//
//            ps.execute();
//            executeJuncQueries(connection, juncQuery, approval);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void executeJuncQueries
//            (Connection connection, String juncQuery, Approval approval) throws SQLException{
//        for (String customer : approval.getCustomers()) {
//            PreparedStatement ps = connection.prepareStatement(juncQuery);
//            ps.setInt(1, approval.getApprovalId());
//            ps.setString(2, customer);
//
//            ps.execute();
//        }
//    }
//
//    @Override
//    public void deleteApproval(Approval approval) {
//        try (Connection connection = DAOUtility.getConnection()) {
//            String juncQuery = "DELETE FROM ApprovalsJunc " +
//                    "WHERE approval_id=?;";
//            String approvalQuery = "DELETE FROM Approvals " +
//                    "WHERE approval_id=?;";
//
//            PreparedStatement ps = connection.prepareStatement(juncQuery);
//            ps.setInt(1, approval.getApprovalId());
//
//            ps.execute();
//
//            ps = connection.prepareStatement(approvalQuery);
//            ps.setInt(1, approval.getApprovalId());
//
//            ps.execute();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//}
