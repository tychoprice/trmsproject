package models.daos;

import models.beans.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDAOImpl implements EmployeeDAO {

//    @Override
//    public List<Employee> getAllEmployees() throws SQLException {
//        List<Employee> employees = new ArrayList<Employee>();
//        Connection conn = null;
//        Statement stmt = null;
//
//        try {
//            conn = DAOUtility.getConnection();
//            stmt = conn.createStatement();
//            String sql = "SELECT * FROM EMPLOYEES";
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                Employee e = new Employee();
//
//                e.setEmployeeId(rs.getInt("employee_id"));
//                e.setUsername(rs.getString("username"));
//                e.setPassword(rs.getString("password"));
//                e.setFirstName(rs.getString("first_name"));
//                e.setLastName(rs.getString("last_name"));
//                e.setEmail(rs.getString("email"));
//
//                employees.add(e);
//            }
//
//            System.out.println(employees.size());
//            System.out.println(employees);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return employees;
//    }

    @Override
    public Employee getEmployeeByName(String username) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = DAOUtility.getConnection();
            String query =
                    "SELECT * " +
                    "FROM Employees " +
                    "WHERE username = ?";
            ps = conn.prepareStatement(query);

            ps.setString(1, username);
            ps.executeQuery();

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Employee employee = new Employee();

                employee.setEmployeeId(resultSet.getInt(1));
                employee.setFirstName(resultSet.getString(2));
                employee.setLastName(resultSet.getString(3));
                employee.setEmail(resultSet.getString(4));
                employee.setUsername(resultSet.getString(5));
                employee.setPassword(resultSet.getString(6));

                return employee;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean doesEmployeeExist(String username) {
        Connection connection = null;
        try {
            connection = DAOUtility.getConnection();
            // Prepare Query
            String query = "SELECT EXISTS(" + "SELECT username " +
                    "FROM Employees " + "WHERE username=?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);

            // Get result set
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public int newEmployee(Employee employee) throws SQLException {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DAOUtility.getConnection();
            String sql = "INSERT INTO EMPLOYEES(username, password, first_name, last_name, email) VALUES(?,?,?,?,?) RETURNING employee_id";

            // Setup PreparedStatement
            ps = connection.prepareStatement(sql);

            // Add parameters from employee into PreparedStatement
            ps.setString(1, employee.getUsername());
            ps.setString(2, employee.getPassword());
            ps.setString(3, employee.getFirstName());
            ps.setString(4, employee.getLastName());
            ps.setString(5, employee.getEmail());

            ResultSet rs = ps.executeQuery();

            rs.next();
            return rs.getInt(1);

        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return -1;

            }
        }
    }

    @Override
    public void deleteEmployee(Employee employee) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet results;

        try {
            connection = DAOUtility.getConnection();

            String sql = "DELETE FROM Employees WHERE username=(?) RETURNING employee_id";

            ps = connection.prepareStatement(sql);
            ps.setString(1, employee.getUsername());

            results = ps.executeQuery();
            results.next();

        } catch (SQLException e) {
            e.printStackTrace();
//            } finally {
//                try {
//                    if (ps != null)
//                        ps.close();
//                    if (connection != null)
//                        connection.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//            }
        }
    }
//            return results;

    @Override
    public void updateEmployee(Employee employee) throws SQLException {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DAOUtility.getConnection();
            // Prepare Queries
            String sql = "UPDATE Employees " +
                    "SET username=?, password=?, first_name=?, last_name=?, email=?" +
                    "WHERE username=?";

            // Prepare employee query
            ps = connection.prepareStatement(sql);
            ps.setString(1, employee.getUsername());
            ps.setString(2, employee.getPassword());
            ps.setString(3, employee.getFirstName());
            ps.setString(4, employee.getLastName());
            ps.setString(5, employee.getEmail());
            ps.setString(6, employee.getUsername());
            // Execute employee query
            ps.execute();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
