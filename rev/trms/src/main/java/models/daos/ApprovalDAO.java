package models.daos;

import models.beans.Approval;

import java.util.List;

public interface ApprovalDAO {

    public abstract boolean doesApprovalExist(int approvalId);
    public abstract int getApprovalCount();
    public abstract Approval getApprovalById(int approvalId);
    public abstract List<Approval> getAllPending();
    public abstract List<Approval> getApprovalsByName(String username);
    public abstract List<Approval> getApprovedApprovalsByName(String username);
    public abstract List<Approval> getPendingApprovalsByName(String username);
    public abstract void newApproval(Approval approval);
    public abstract void updateApproval(Approval approval);
    public abstract void deleteApproval(Approval approval);

}
