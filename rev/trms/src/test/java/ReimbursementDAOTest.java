//import models.beans.Reimbursement;
//import models.daos.ReimbursementDAOImpl;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.sql.SQLException;
//
//public class ReimbursementDAOTest {
//
//    private Reimbursement reimbursement = new Reimbursement();
//
//    private ReimbursementDAOImpl reimbursementDao = new ReimbursementDAOImpl();
//
//    @Before
//    public void setReimbursement() {
//        reimbursement.setCourseType("certification");
//        reimbursement.setStartDate(12,12,2018);
//        reimbursement.setLocation("online");
//        reimbursement.setDescription("java oca exam");
//        reimbursement.setReason("requirement");
//        reimbursement.setAttachment();
//        reimbursement.setWorkMissed(0);
//    }
//
//    @After
//    public void releaseReimbursement() {
//        Reimbursement reimbursement = new Reimbursement();
//    }
//
//    @Test
//    public void testNewReimbursement() {
//
//        try {
//            reimbursementDao.newReimbursement(reimbursement);
//            Assert.assertTrue(reimbursementDao.doesReimbursementExist(reimbursement.getReimbursementId()));
//            reimbursementDao.deleteReimbursement(reimbursement);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testDeleteReimbursement() {
//
//        try {
//            reimbursementDao.newReimbursement(reimbursement);
//            reimbursementDao.deleteReimbursement(reimbursement);
//            Assert.assertFalse(reimbursementDao.doesReimbursementExist(reimbursement.getUsername()));
//        } catch (SQLException e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
//
//    @Test
//    public void testUpdateReimbursement() {
//
//        try {
//            int iReimbursement;
//            reimbursementDao.newReimbursement(reimbursement);
//            reimbursementDao.updateReimbursement(reimbursement);
//
//            Assert.assertTrue(reimbursementDao.doesReimbursementExist(reimbursement.getUsername()));
//            Reimbursement e = reimbursementDao.getReimbursementByName(reimbursement.getUsername());
//            Assert.assertEquals(reimbursementDao.getReimbursementByName(reimbursement.getUsername()), e);
//            reimbursementDao.deleteReimbursement(reimbursement);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testGetReimbursementByName() {
//
//        try {
//            int i;
//            i = reimbursementDao.newReimbursement(reimbursement);
//            Assert.assertTrue(reimbursementDao.doesReimbursementExist(reimbursement.getUsername()));
//            reimbursement.setReimbursementId(i);
//            Reimbursement e = reimbursementDao.getReimbursementByName(reimbursement.getUsername());
//            Assert.assertEquals(reimbursementDao.getReimbursementByName(reimbursement.getUsername()), e);
//            reimbursementDao.deleteReimbursement(reimbursement);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            Assert.fail();
//        }
//    }
//}
