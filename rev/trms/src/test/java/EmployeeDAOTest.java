import models.beans.Employee;
import models.daos.EmployeeDAOImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class EmployeeDAOTest {

    private Employee employee = new Employee();

    private EmployeeDAOImpl employeeDao = new EmployeeDAOImpl();

    @Before
    public void setEmployee() {
        employee.setUsername("asdfasdf");
        employee.setPassword("asdfasdf123");
        employee.setFirstName("asdf");
        employee.setLastName("asdf");
        employee.setEmail("asdfasdf@asdfasdf.com");
    }

    @After
    public void releaseEmployee() {
        Employee employee = new Employee();
    }

    @Test
    public void testNewEmployee() {

        try {
            employeeDao.newEmployee(employee);
            Assert.assertTrue(employeeDao.doesEmployeeExist(employee.getUsername()));
            employeeDao.deleteEmployee(employee);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteEmployee() {

        try {
            employeeDao.newEmployee(employee);
            employeeDao.deleteEmployee(employee);
            Assert.assertFalse(employeeDao.doesEmployeeExist(employee.getUsername()));
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testUpdateEmployee() {

        try {
            int iEmployee;
            employeeDao.newEmployee(employee);
            employeeDao.updateEmployee(employee);

            Assert.assertTrue(employeeDao.doesEmployeeExist(employee.getUsername()));
            Employee e = employeeDao.getEmployeeByName(employee.getUsername());
            Assert.assertEquals(employeeDao.getEmployeeByName(employee.getUsername()), e);
            employeeDao.deleteEmployee(employee);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetEmployeeByName() {

        try {
            int i;
            i = employeeDao.newEmployee(employee);
            Assert.assertTrue(employeeDao.doesEmployeeExist(employee.getUsername()));
            employee.setEmployeeId(i);
            Employee e = employeeDao.getEmployeeByName(employee.getUsername());
            Assert.assertEquals(employeeDao.getEmployeeByName(employee.getUsername()), e);
            employeeDao.deleteEmployee(employee);

        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
//
//    @Test
//    public void testBooleanEmployee() {
//
//    }
