package beans;

import models.beans.Employee;
import org.junit.Assert;
import org.junit.Test;

public class EmployeeTest {
    @Test
    public void testEmployee() {
        String password = "password";
        Employee test = new Employee();
        test.setPassword(password);
        Assert.assertEquals(test.getPassword(), password);
    }
}
